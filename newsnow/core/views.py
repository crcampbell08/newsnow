from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.conf import settings

import requests

from .serializer import ArticleSerializer


def save_article(request):

    # need to save 100 articles not just one
    url = ('http://newsapi.org/v2/top-headlines?'
        'country=us')
    headers = {'X-API-Key': settings.NEWS_API_KEY}
    response = requests.get(url, headers=headers)
    json = response.json()
    # parse JSON here before you pass into serializer?
    serializer = ArticleSerializer(data=json)
    if serializer.is_valid():
        article = serializer.save()
        return render(request, 'home.html', {'articles': articles})
    # return JsonResponse(serializer.data, safe=False)
    return render(request, 'invalid.html')
