from django.db import models

class Article(models.Model):
    # source = models.TextField()
    # author = models.TextField()
    # title = models.CharField(max_length=200)
    # description = models.TextField()
    # url = models.TextField()
    # urlToImage = models.TextField()
    # publishedAt = models.DateField() 
    # content = models.TextField()

    id = models.IntegerField(primary_key=True)
    source = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    description = models.TextField()
    url = models.URLField()
    urlToImage = models.CharField(max_length=200)
    publishedAt = models.DateField()
    content = models.CharField(max_length=200)


    # source': {'id': None, 'name': 'Npr.org'},
    #  'author': '', 
    #  'title': 'CDC Recommends Against Gatherings Of 50 Or More, As States Close Bars And Restaurants - NPR', 
    #  'description': 'The Centers for Disease Control and Prevention said it is warning ', 
    #  'url': 'https://www.npr.org/2020/03/15/816245252/cdc-recommends-suspending-gatherings-of-50-or-more-people-for-the-next-8-weeks', 
    #  'urlToImage': 'https://media.npr.org/assets/img/2020/03/15/gettyimages-1207243271_wide-b6a9c6a779230513fce2eb0b6181403db9db0563.jpg?s=1400',
    #  'publishedAt': '2020-03-16T06:02:45Z', 
    #  'content': 'Hairless Hare Brewery in Vandalia, Ohio, before Gov. Mike DeWine ordered a mandatory shutdown of all bars and restaurants starting at 9 p.m. on Sunday.\r\nBrad Lee/AFP via Getty Images\r\nUpdated at 1:55 a.m. ET Monday\r\nIn an effort to slow the spread of the coro… [+5406 chars]'}