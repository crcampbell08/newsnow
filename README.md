**Getting started**

# Make sure you have Python 3.0 or greater installed
    $ python -V
# Make sure pip command is using newest python version


# Create a virtual environment
$ python3 -m venv env

# Install Django and Django REST framework into the virtual environment
pip install django
pip install djangorestframework


# verify Django version 3.0+ was installed properly
    $ python -m django --version

# navigate to NEWNOW/code dir where manage.py is located
# run to start server
    $ python manage.py runserver

# then navigate to port 8000 in your browser
# http://127.0.0.1:8000/